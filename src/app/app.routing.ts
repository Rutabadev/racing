import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RacingComponent } from './racing/racing.component';
import { PiedComponent } from './racing/pied/pied.component';
import { VoitureComponent } from './racing/voiture/voiture.component';
import { AddComponent } from './racing/pied/add/add.component';

const appRoutes: Routes = [
  { path: 'home',
    component: RacingComponent,
    children: [
      {
        path: 'pied',
        component: PiedComponent,
        children: [
          {
            path: 'add',
            component: AddComponent
          }
        ]
      },
      {
        path: 'voiture',
        component: VoitureComponent
      }
    ]
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class RoutingModule { }
