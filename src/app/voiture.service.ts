import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Voiture } from './voiture';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class VoitureService {

  constructor(private http: Http) { }

  getTutures() {
    return this.http.get('../assets/voitures.json')
      .map(response => <Voiture[]>response.json().voituresData)
      .delay(5000);
  }

}
