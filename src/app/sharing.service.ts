import { Injectable } from '@angular/core';
import { data } from './data';

@Injectable()
export class SharingService {

  constructor() { }

  data1 = data;

  data2 = {
    name: 'Jean',
    lastname: 'Louis'
  };

}
