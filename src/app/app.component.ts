import { Component } from '@angular/core';
import { SharingService } from './sharing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  sharing: SharingService;
    constructor(sharingService: SharingService) {
      this.sharing = sharingService;
    }
}
