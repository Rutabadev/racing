import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MaterializeModule } from 'angular2-materialize';

import { AppComponent } from './app.component';
import { RacingModule } from './racing/racing.module';
import { RoutingModule } from './app.routing';
import { SharingService } from './sharing.service';
import { HeaderComponent } from './header/header.component';
import { VoitureService } from './voiture.service';
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    MaterializeModule,
    BrowserModule,
    FormsModule,
    RoutingModule,
    RacingModule,
    HttpModule
  ],
  providers: [SharingService],
  bootstrap: [AppComponent]
})
export class AppModule { }
