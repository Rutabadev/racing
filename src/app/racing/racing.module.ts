import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PiedComponent } from './pied/pied.component';
import { VoitureComponent } from './voiture/voiture.component';
import { RacingComponent } from './racing.component';
import { RoutingModule } from '../app.routing';
import { LeetPipe } from './pied/leet.pipe';
import { FormsModule } from '@angular/forms';
import { OrteilComponent } from './pied/orteil/orteil.component';
import { AddComponent } from './pied/add/add.component';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormsModule
  ],
  exports: [
    RacingComponent,
    PiedComponent,
    VoitureComponent
  ],
  declarations: [
    RacingComponent,
    PiedComponent,
    VoitureComponent,
    LeetPipe,
    OrteilComponent,
    AddComponent]
})
export class RacingModule { }
