import { Component, OnInit, Input } from '@angular/core';
import { SharingService } from '../sharing.service';

@Component({
  selector: 'app-racing',
  templateUrl: './racing.component.html',
  styleUrls: ['./racing.component.scss'],
  providers: []
})
export class RacingComponent implements OnInit {
  sharing: SharingService;

  constructor(sharingService: SharingService) {
    this.sharing = sharingService;
  }

  ngOnInit() {
  }

}
