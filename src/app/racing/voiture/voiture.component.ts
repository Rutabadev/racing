import { Component, OnInit, Input } from '@angular/core';
import { SharingService } from '../../sharing.service';
import { VoitureService } from '../../voiture.service';
import { Voiture } from '../../voiture';

@Component({
  selector: 'app-voiture',
  templateUrl: './voiture.component.html',
  styleUrls: ['./voiture.component.scss'],
  providers: [VoitureService]
})
export class VoitureComponent implements OnInit {
  loading = true;
  voituresService: VoitureService;
  sharing: SharingService;
  tutures: Voiture[];

  constructor(sharingService: SharingService, voitureService: VoitureService) {
    this.sharing = sharingService;
    this.voituresService = voitureService;
  }

  ngOnInit() {
    this.voituresService.getTutures().subscribe(tutures => {
        this.tutures = tutures;
        this.loading = false;
      }
    );
  }

}
