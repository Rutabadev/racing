import { Injectable } from '@angular/core';
import { PIEDS } from './pied.mock';

@Injectable()
export class PiedsService {

  constructor() { }

  pieds = PIEDS;

}
