import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-orteil',
  templateUrl: './orteil.component.html',
  styleUrls: ['./orteil.component.scss']
})
export class OrteilComponent implements OnInit {
  _orteil: object;
  @Input()
  set orteil(orteil: object) {
    this._orteil = orteil;
  }
  constructor() { }

  ngOnInit() {
  }

}
