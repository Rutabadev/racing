import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrteilComponent } from './orteil.component';

describe('OrteilComponent', () => {
  let component: OrteilComponent;
  let fixture: ComponentFixture<OrteilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrteilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrteilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
