import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'leet'
})
export class LeetPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let re = value;
    re = re.replace('e', '3');
    re = re.replace('l', '1');
    re = re.replace('o', '0');
    re = re.replace('t', '7');
    re = re.replace('d', '|)');
    re = re.replace('/2', '0');
    return re;
  }

}
