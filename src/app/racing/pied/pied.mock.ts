export const PIEDS = [
  {
    name: 'gauche',
    fonctionnel: true,
    orteils: [
      {
        name: 'tinky'
      },
      {
        name: 'winky'
      },
      {
        name: 'diepsy'
      },
      {
        name: 'po'
      }
    ]
  },
  {
    name: 'droit',
    fonctionnel: false,
    orteils: [
      {
        name: 'oid'
      },
      {
        name: 'douc'
      },
      {
        name: 'pouce'
      }
    ]
  },
  {
    name: 'chaussé',
    fonctionnel: true
  }
];
