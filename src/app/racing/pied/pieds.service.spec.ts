import { TestBed, inject } from '@angular/core/testing';

import { PiedsService } from './pieds.service';

describe('PiedsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PiedsService]
    });
  });

  it('should be created', inject([PiedsService], (service: PiedsService) => {
    expect(service).toBeTruthy();
  }));
});
