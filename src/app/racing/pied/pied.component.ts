import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PiedsService } from './pieds.service';

@Component({
  selector: 'app-pied',
  templateUrl: './pied.component.html',
  styleUrls: ['./pied.component.scss'],
  providers: [PiedsService]
})
export class PiedComponent implements OnInit {
  pieds: ({ name: string; fonctionnel: boolean; orteils: { name: string; }[]; } | { name: string; fonctionnel: boolean; })[];

  constructor(private _router: Router, private r: ActivatedRoute, private piedService: PiedsService) {
    this.pieds = piedService.pieds;
  }

  ngOnInit() {
  }

  addIep() {
    this._router.navigate(['./add'], { relativeTo: this.r });
  }

}
