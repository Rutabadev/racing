import { Component, OnInit } from '@angular/core';
import { PiedsService } from '../pieds.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  newPied: any;
  newOrteil: any;
  pieds: any;

  constructor(private piedsService: PiedsService) {
    this.pieds = piedsService.pieds;
   }

  ngOnInit() {
    this.resetPied();
    this.resetOrteil();
  }

  private resetOrteil() {
    this.newOrteil = new Object;
  }

  private resetPied() {
    this.newPied = new Object;
    this.newPied.fonctionnel = true;
    this.newPied.orteils = [];
  }

  addPied() {
    this.pieds.push(this.newPied);
    this.resetPied();
  }

  addOrteil() {
    this.newPied.orteils.push(this.newOrteil);
    this.resetOrteil();
  }

}
